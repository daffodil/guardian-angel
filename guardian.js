
// declare global, pete spits
var map = 0;

var GOOGLE_MAP_API_KEY = "AIzaSyD0FEYAMHAcq6Rru5ZIx0LMIQfOnfQOmbY";

// This is the basic data and manual for now to build..

// the pots are the approach to each radar_points
// the [lat, lon] ie north, south are the points
// danger zone is the "collision point"
// decision_point is the drivers decision to go
var DATA = {}
DATA.project =  "Penrhiw";
DATA.danger_point =  [51.843239, -3.987188];
DATA.decision_point =  [51.843262, -3.987134];
DATA.radar_points =  [
	[51.843142, -3.986807],
	//[51.843200, -3.986874],
	[51.843508, -3.987685]
];
DATA.plots =  [
	[
		[51.843423, -3.985468], [51.843312, -3.985963], [51.843172, -3.986569], [51.843156, -3.986773], [51.843166, -3.986965], [51.843210, -3.987129]
	],
	[
		[51.843624, -3.988646], [51.843542, -3.987979], [51.843538, -3.987845], [51.843470, -3.987601], [51.843401, -3.987433], [51.843286, -3.987233]
	]
];
//console.log("DATA=", DATA);
function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(DATA.danger_point[0], DATA.danger_point[1]),
          zoom: 18,
		  mapTypeId: google.maps.MapTypeId.SATELLITE
        };
		// reinit map here..
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		
		
		
		add_marker(DATA.danger_point, "Collision");
		add_marker(DATA.decision_point, "Decision");
		add_marker(DATA.radar_points[0], "Radar");
		add_marker(DATA.radar_points[1], "Radar");
		
		add_approach(DATA.plots[0], '#FFFF00');
		add_approach(DATA.plots[1], '#FFFF00');
		
		add_danger_lines(DATA.plots[0]);
		add_danger_lines(DATA.plots[1]);
		
		add_zone_of_confusion(DATA, 0);
		add_zone_of_confusion(DATA, 1);
		
		//add_line(DATA.plots[0][2,6], '#FFFF00');
		//add_line(DATA.plots[1][0,3], '#FFFF00');
}

function add_marker(pnt, label){
	var marker = new google.maps.Marker({
      position: new google.maps.LatLng(pnt[0], pnt[1]),
      map: map,
      title: label
  });
}

function add_approach(points, color){
	
	// this is the approach points to radar
	var approach_coords = [];
	var i = 0;
	for(i = 0; i < 3; i++){
		approach_coords.push(
			new google.maps.LatLng( points[i][0], points[i][1] )
		);
	};
	var flightPath = new google.maps.Polyline({
		path: approach_coords,
		geodesic: true,
		strokeColor: '#FFFF00',
		strokeOpacity: 1.0,
		strokeWeight: 2
	});
	flightPath.setMap(map);
	

}

function add_danger_lines(points, color){
	
	// this is the approach points to radar
	var coords = [];
	var i = 0;
	for(i = 3; i < points.length; i++){
		coords.push(
			new google.maps.LatLng( points[i][0], points[i][1] )
		);
	};
	var path = new google.maps.Polyline({
		path: coords,
		geodesic: true,
		strokeColor: '#F46E55',
		strokeOpacity: 1.0,
		strokeWeight: 2
	});
	path.setMap(map);
}
function add_zone_of_confusion(data, idx){
	var coords = [
		new google.maps.LatLng( data.plots[idx][2][0], data.plots[idx][2][1] ),
		new google.maps.LatLng( data.plots[idx][3][0], data.plots[idx][3][1] )
	];
	var path = new google.maps.Polyline({
		path: coords,
		geodesic: true,
		strokeColor: '#F455CA',
		strokeOpacity: 1.0,
		strokeWeight: 2
	});
	path.setMap(map);
}

